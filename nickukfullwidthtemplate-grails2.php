<?php
function microsites_pageprocess($title, $headers) {
    $domain = 'http://www.nickelodeon.ru';
    $url = $domain . '/microsite/microsite-template/gc15li'; //this is the URL for the page that contains just the header and footer code
    $first_delimiter = "<section class=\"mtvn-lyt mtvn-lyt-4-2\">"; //microsite content should appear AFTER this tag
    $second_delimiter = "</section>";

    // Add code to fetch from Memcache
    // If not in memcache then fetch:
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $page = curl_exec($ch);
    curl_close($ch);

    // Error checking - if couldnt get from remote server then grab local file cache copy.

    $page = str_replace('src="/', 'src="' . $domain . '/', $page); //add the domain to any CSS/JS file paths
    $page = str_replace('href="/', 'href="' . $domain . '/', $page); //add the domain to any CSS/JS file paths
    $page = str_replace('<title>MICROSITE WRAPPER Микросайт Nickelodeon | Nick Russia</title>', '<title>' . $title . '</title>', $page); // Modify page title
    $headerreplace = $headers . "</head>";
    $page = str_replace('</head>',$headerreplace,$page);  // Add any additional headers to the page
    $page_components1 = explode($first_delimiter, $page); //separate the string into an array of two strings, the first is the header and the second is the footer
    $page_values['header'] = $page_components1[0] . "<section class=\"mtvn-lyt mtvn-lyt-6\">";
    $page_components2 = explode($second_delimiter, $page);
    $page_values['footer'] = "</section>" . $page_components2[1];
    return ($page_values);
}