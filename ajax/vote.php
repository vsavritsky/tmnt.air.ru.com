<?php

if (!empty($_POST['vote'])) {
    $filePath = __DIR__.'/'.md5($_POST['vote']).'.count';
    if (!file_exists($filePath)) {
        echo 'error file_exists'; exit();
    }

    $count = (int)file_get_contents($filePath);
    if (!$count) {
        echo 'error read count'; exit();
    }

    $count++;
    $record = file_put_contents($filePath, $count);
    if (!$record) {
        echo 'error write count'; exit();
    }

    $log = date("Y-m-d H:i:s").';'.$_POST['vote'].';'.$_SERVER['REMOTE_ADDR'].';'.$_SERVER['HTTP_USER_AGENT'].';'.PHP_EOL;
    $record = file_put_contents('full_vote.csv', $log, FILE_APPEND);
    if (!$record) {
        echo 'error write log'; exit();
    }
} else {
   echo 'error post error'; exit(); 
}
