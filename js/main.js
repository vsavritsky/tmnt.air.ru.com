(function($) {
    var wwidth = $( window ).width();
    if (wwidth > 480) {
        wwidth = 480;
    }

    $('body').on('click', '.work.button', function() {
        $.colorbox({width: wwidth, opacity: 0.8, html: $('.work.error.close').html()})
        return false;

        var work = $(this).parents('.work-wrap').find('.work');
        var imageId = work.data('id');
        if ($.cookie('vote_nic')) {
            $.colorbox({width: wwidth, opacity: 0.8, html: $('.work.error').html()})
        } else {
            $.ajax({
                method: "POST",
                url: location.href+"ajax/vote.php",
                data: { vote: imageId }
            })
                .done(function( msg ) {
                    $.cookie('vote_nic', imageId, { expires: 365, path: '/' });
                    $.colorbox({width: wwidth, opacity: 0.8, html: $('.work.success').html()});
                    $('.participate').css('opacity', 1);
                    var count = parseInt(work.find('.counter').html());
                    work.find('.counter').html(count+1);
                });
        }
    })

    $('body').on('click', '.participate.button', function() {
        if (!$.cookie('vote_nic')) {
            $.colorbox({width: wwidth, opacity: 0.8, html: $('.participate.error').html()})
        } else {
            location.href=$(this).data('url');
        }
    })

})(jQuery);
