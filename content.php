<link href="http://allfont.ru/allfont.css?fonts=rodchenkoctt" rel="stylesheet" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://www.jacklmoore.com/colorbox/jquery.colorbox.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/main.js?sdfs"></script>
<link rel="stylesheet" href="./css/colorbox.css" />
<link rel="stylesheet" href="./css/style.css" />

<?
$images = array();
$counts = array();
foreach (glob("./images_contest/*.png") as $filename) {
    $images[basename($filename)] = $filename;
    $countFilePath = __DIR__.'/ajax/'.md5(basename($filename)).'.count';
    $counts[basename($filename)] = 0;
    if (file_exists($countFilePath)) {
        $counts[basename($filename)] = (int)file_get_contents($countFilePath);
    }
}
?>

<div id="container">
    <div id="content">
        <div class="description">

            <img id="header" src="./images/header.png">
            Голосование завершено! Результаты будут опубликованы в <a href="http://nickelodeon.ru/news">новостях</a> 22 сентября <br><br><br><br><br>
        </div>

        <div class="works">
            <? foreach ($images as $id => $image) : ?>
                <div class="work-wrap">
                    <div class="work" data-id="<?=$id?>">
                        <img src="<?=$image?>">

                        <div class="counter">
                            <?=$counts[$id]?>
                        </div>
                    </div>
                    <div class="work button">Голосовать</div>
                </div>
            <? endforeach; ?>
        </div>

        <!--<div class="participate button <?=(!empty($_COOKIE['vote_nic']) ? 'show' : '')?>"  data-url="http://vk.com/topic-27078945_32515680">Участвовать в конкурсе</div>-->
    </div>

    <div class="message-popup">

    </div>

    <div class="participate error hide">
        Чтобы участвовать в конкурсе, сначала тебе нужно проголосовать
    </div>

    <div class="work error close hide">
        Голосование завершено
    </div>

    <div class="work error hide">
        Ты уже проголосовал!
    </div>

    <div class="work success hide">
        Твой голос учтен!
    </div>
</div>
